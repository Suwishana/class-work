package bcas.ap.inter;

public interface Shape {

public double getArea(double w, double h);

		public double getPerimeter(double w, double h);

		public String getColour();

		int getEdges();

		boolean isFilled();
	}


