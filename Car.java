

package bcas.ap.inter;

public interface Car {
	public String getBody();

	public String getColour();

	public int getAnnualCost();

}


