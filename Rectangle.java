package bcas.ap.inter;

public class Rectangle implements Shape {
	@Override
	public double getArea(double w, double h) {
		return w*h;
	}
	@Override
	public double getPerimeter(double w, double h) {
		return 2* (w+h);
		
	}
	
	@Override
	public String getColour() {
		return "Red";
	}
		
	
	@Override
	public boolean isFilled() {
		return false;
	}
	@Override
	public int getEdges() {
		return 4;
	
	}
}


